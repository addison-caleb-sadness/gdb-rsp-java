package info.addisoncrump.vr.gdb.rsp;

import org.eclipse.che.api.debugger.server.exceptions.DebuggerException;
import org.eclipse.che.plugin.gdb.server.exception.GdbTerminatedException;
import org.eclipse.che.plugin.gdb.server.parser.GdbOutput;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class AddingNumbersGDBTest {

    private BasicGdb gdb;

    @Before
    public void setup() throws IOException, GdbTerminatedException, InterruptedException {
        this.gdb = new BasicGdb();
        gdb.grabGdbOutput();
    }

    @Test
    public void testAddingNumbersRun() throws IOException, GdbTerminatedException, InterruptedException {
        GdbOutput output = gdb.sendCommand("file " + GDBTestSuite.getBinaryFolder().resolve("adding-numbers").toAbsolutePath().toString());
        assertThat(output.getOutput(), Matchers.matchesPattern("Reading symbols from .*adding-numbers...(?:done.)?\n"));
        output = gdb.sendCommand("run");
        System.out.println(output.getOutput());
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(output.getOutput().getBytes())))) {
            reader.readLine(); // Starting program: /tmp/gdb-rsp-tests.../adding-numbers
            reader.readLine(); // [Thread debugging using libthread_db enabled]
            reader.readLine(); // Using host libthread_db library "/lib/x86_64-linux-gnu/libthread_db.so.1".
            assertEquals("5 + 6 = 11", reader.readLine()); // output line
        }
    }

    @Test
    public void testAddingNumbersBreakable() throws IOException, GdbTerminatedException, InterruptedException {
        GdbOutput output = gdb.sendCommand("file " + GDBTestSuite.getBinaryFolder().resolve("adding-numbers").toAbsolutePath().toString());
        assertThat(output.getOutput(), Matchers.matchesPattern("Reading symbols from .*adding-numbers...(?:done.)?\n"));
        output = gdb.sendCommand("break add");
        assertThat(output.getOutput(), Matchers.matchesPattern("Breakpoint 1 at 0x[0-9a-f]+\n"));
        output = gdb.sendCommand("run");
        assertThat(output.getOutput(), Matchers.matchesPattern("(.*\\n)+Breakpoint 1, .* in add \\(\\)\n"));
    }

    @After
    public void tearDown() throws InterruptedException, IOException, DebuggerException {
        gdb.stop();
    }


}
