package info.addisoncrump.vr.gdb.rsp;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@RunWith(JUnitPlatform.class)
@Suite.SuiteClasses({AddingNumbersTest.class})
public class GDBTestSuite {

    private static Path binFolder;

    public static Path getBinaryFolder() throws IOException {
        if (binFolder != null) {
            return binFolder;
        }
        binFolder = Files.createTempDirectory("gdb-rsp-tests");
        binFolder.toFile().deleteOnExit();

        Path binary = binFolder.resolve("adding-numbers");

        Files.copy(GDBTestSuite.class.getResourceAsStream("/adding-numbers"), binary);

        if (!binary.toFile().setExecutable(true)) {
            throw new RuntimeException("Couldn't mark adding-numbers as executable.");
        }
        return binFolder;
    }

}
