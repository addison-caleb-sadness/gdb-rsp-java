package info.addisoncrump.vr.gdb.rsp;

import org.junit.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AddingNumbersTest {

    @Test
    public void testAddingNumbers() throws IOException, InterruptedException {
        ProcessBuilder builder = new ProcessBuilder();
        Process proc = Runtime.getRuntime().exec(new String[]{"./adding-numbers"}, new String[]{}, GDBTestSuite.getBinaryFolder().toFile());
        proc.waitFor();
        String output = new String(proc.getInputStream().readAllBytes(), StandardCharsets.UTF_8);
        assertEquals("5 + 6 = 11\n", output);
    }

}
