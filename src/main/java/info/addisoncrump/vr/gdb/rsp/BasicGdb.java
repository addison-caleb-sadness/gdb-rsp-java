package info.addisoncrump.vr.gdb.rsp;

import org.eclipse.che.plugin.gdb.server.GdbProcess;
import org.eclipse.che.plugin.gdb.server.exception.GdbTerminatedException;
import org.eclipse.che.plugin.gdb.server.parser.GdbOutput;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class BasicGdb extends GdbProcess {
    private static final Logger LOG = LoggerFactory.getLogger(GdbProcess.class);
    private static final String PROCESS_NAME = "gdb";
    private static final String OUTPUT_SEPARATOR = "(gdb) ";

    public BasicGdb() throws IOException {
        super(OUTPUT_SEPARATOR, PROCESS_NAME);
    }

    // BEGIN MODIFIED SECTION FROM org.eclipse.che.plugin.gdb.server.Gdb
    // Copyright copied from pom of original project:
    /*

    Copyright (c) 2012-2018 Red Hat, Inc.
    This program and the accompanying materials are made
    available under the terms of the Eclipse Public License 2.0
    which is available at https://www.eclipse.org/legal/epl-2.0/

    SPDX-License-Identifier: EPL-2.0

    Contributors:
      Red Hat, Inc. - initial API and implementation

     */
    // Available here: https://search.maven.org/artifact/org.eclipse.che.plugin/che-plugin-gdb-server/6.19.6/jar

    public GdbOutput sendCommand(@NotNull String command)
            throws IOException, GdbTerminatedException, InterruptedException {
        return sendCommand(command, true);
    }

    public synchronized GdbOutput sendCommand(@NotNull String command, boolean grabOutput)
            throws IOException, GdbTerminatedException, InterruptedException {
        LOG.debug(command);

        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));
        writer.write(command);
        writer.newLine();
        writer.flush();

        return grabOutput ? grabGdbOutput() : null;
    }

    @NotNull
    public GdbOutput grabGdbOutput() throws InterruptedException, GdbTerminatedException {
        GdbOutput gdbOutput = outputs.take();
        if (gdbOutput.isTerminated()) {
            String errorMsg = "GDB has been terminated with output: " + gdbOutput.getOutput();
            LOG.error(errorMsg);
            throw new GdbTerminatedException(errorMsg);
        }
        return gdbOutput;
    }

    // END TAKEN SECTION FROM org.eclipse.che.plugin.gdb.server.Gdb

    @Override
    public void stop() {
        super.stop();
    }
}
